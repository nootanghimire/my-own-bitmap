My Own (and Non-Standard) Bitmap Reader/Writer
=============

Did this just for fun. Worked with the idea of [Niraj Khadka](http://twitter.com/N3Ur0t0x1c) and coded independently. 


What for?
====

This program writes bit-map (or bit information) to a binary file and use another program to extract those information and plot on the 
screen. 


I wrote this program when i learned about how bitmaps worked (written on Feb 2013). 


Actually, this program was written to compile in Turbo C. So, if your old computer has it till now, you can use it :P (I tested on virtual WinXP)


Do not forget to include graphics.h and do some minor edits  before running the program. Currently it displays debug information only. (texts instead of pictures, ;) )

That's all. 



License
=====

Well, I like this [DBAD](https://github.com/philsturgeon/dbad) license, and this code is released on it.
